/// <reference types="Cypress" />
import 'cypress-file-upload';

let jurajCidorikIntro = `Dear Madam, Dear Sir,
Based on the job description for Senior test automation engineer, I understand, you are looking for the person, who will be responsible for the creation of the automated e2e tests in Cypress. Furthermore, you would like to incorporate those tests in your CI/CD pipeline in GitLab. In order to save your valuable time and to show you, how much I would like to be part of your team, I took the time and prepared something for you. It is a project you can use to assess my skills in regards to Cypress and GitLab. It is available at: https://gitlab.com/juraj-cidorik/exponea . Should you see it suitable, I would be very happy to demonstrate it to you.
If you would see me ﬁt to a job description, you might ask next: “Will he be any good to our company in years to come?”. My humble answer is: “Yes”. In any position, I have been, I have never stopped learning. That is the way, how I learnt to code in JavaScript and Python while doing manual testing. It is my attitude to seek or create and implement cutting-edge frameworks, technologies and knowledge to continually improve my work. In my approach to work challenges, I refuse to seek reasons why something could not be done. Instead, I use the time to ﬁnd a solution or gain experience. Culture of Exponea feels like a great ﬁt for me..
Integrity and friendliness are words I would use to describe my personality. Work relationships are to me something more, than just a means to an end. Beyond completing tasks that need to be done, I enjoy meeting my colleagues in after work hours in a casual atmosphere.
My years of experience, technical skills and personality will serve you well , should you decide to take me on board.
I would like to kindly ask you to confirm receipt of my application for the role.
Yours sincerely,
Juraj Cidorík`

describe("Apply for senior test automation enginier", () => {
    
    it("Open exponea homepage", () => {
        cy.visit('/')
        cy.contains('Sell To Customers')
    })

    it('Go to Company', () => {
        cy.get(':nth-child(6) > .dd > span').click()
    })

    it("Open Careers", () => {
        cy.get('.active > .group > ul > :nth-child(3) > a > strong').click()
    })
    it("Choose Senior test automation engineer position", () => {
        cy.contains("Senior test automation engineer").click()
    })
    it("Fill in my name", () => {
        cy.get('.name-1 > .wpcf7-form-control').type("Juraj Cidorík") 
    })

    it("Fill in my email", () => {
        cy.get('.email > .wpcf7-form-control').type("cidorikjuraj@gmail.com") 
    })

    it("Upload my CV", () => {
        cy.get(':nth-child(2) > .wpcf7-form-control-wrap > .wpcf7-form-control').attachFile('Juraj_Cidorik_CV.pdf')
    })
    
    it("Enter my introduction", () => {
        cy.get(':nth-child(10) > .wpcf7-form-control-wrap > .wpcf7-form-control').type(jurajCidorikIntro) 
    })

    it("Agree with T&C", () => {
        cy.get('.wpcf7-list-item > input').check()
    })

    it("Send Application", () => {
        cy.get('.text-center > .wpcf7-form-control').click()
    })
})